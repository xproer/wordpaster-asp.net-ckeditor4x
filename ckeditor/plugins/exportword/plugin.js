﻿CKEDITOR.plugins.add('exportword',
{
	init: function(editor)
	{
		editor.addCommand('exportword',
		{
			exec: function(editor)
            {
				window.zyOffice.SetEditor(editor).api.exportWord();
			}
		});
		editor.ui.addButton('exportword',
		{
			label: '导出Word文档（docx）',
			command: 'exportword',
			icon: this.path + 'images/z.png'
		});
	}
});
