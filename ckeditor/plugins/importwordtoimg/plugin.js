﻿CKEDITOR.plugins.add('importwordtoimg',
{
	init: function(editor)
	{
		editor.addCommand('importwordtoimg',
		{
			exec: function(editor)
            {
                WordPaster.getInstance().SetEditor(editor);
				WordPaster.getInstance().importWordToImg();
			}
		});
		editor.ui.addButton('importwordtoimg',
		{
			label: 'Word转图片',
			command: 'importwordtoimg',
			icon: this.path + 'images/word1.png'
		});
	}
});
